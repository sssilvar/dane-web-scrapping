from lxml import html

import httpx
import pandas as pd

from .nlp import preprocess_text


def scrap_site(url, sep_docs=True, **kwargs):
    """ Perform web scraping over url.

    Arguments:
        url {str} -- URL of the site

    Keyword Arguments:
        sep_docs {bool} -- Separated documents based on their tags (default: {False})
    """
    raw_html = httpx.get(url, verify=False, **kwargs).text
    tree = html.fromstring(raw_html)
    results = tree.xpath('//*[not(self::script or self::style)]/text()')
    results = [r.lstrip().rstrip() for r in results]  # Remove brealkines and junk spaces at begining/end
    results = [r for r in results if r]  # Remove empty results

    # Preprocess text
    # results = [preprocess_text(r) for r in results]

    if sep_docs:
        return pd.Series(name='text', data=results)
    else:
        return ' '.join(results)

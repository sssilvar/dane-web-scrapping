from .scrappers import scrap_site
from .nlp import get_keywords
from .ui import dataframe_to_markdown

def test():
    # Scrap site
    url = 'https://covid-19-response.unstatshub.org/useful-links/resources-from-national-statistical-offices/'
    results = scrap_site(url, timeout=20)

    # Extract Keywords
    keywords = get_keywords(results)
    keywords.to_csv('/tmp/keywords.csv')
    
    # Parse dataframe as Dash object
    return dataframe_to_markdown(keywords)
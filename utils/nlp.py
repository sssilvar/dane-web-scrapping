import os
import spacy
import pytextrank
import texthero as hero

# spacy.cli.download('en_core_web_sm')
model_names = {
    'Español': 'es_core_news_sm',
    'Inglés': 'en_core_web_sm'
}
spacy_models = {lang: spacy.load(model_name) for lang, model_name in model_names.items()}
nlp = spacy_models['Inglés']

# Add PyTextRank to the spaCy pipeline
try:
    tr = pytextrank.TextRank()
    nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)
except ValueError:
    pass


def reload_model(lang):
    spacy_lang_model = model_names[lang]
    global nlp
    if not spacy_lang_model.startswith(nlp.lang):
        nlp = spacy_models[lang]
        # Add PyTextRank to the spaCy pipeline
        tr = pytextrank.TextRank()
        nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)


def preprocess_text(text, pos_tags=['PROPN', 'NOUN', 'ADJ']):
    """Text preprocessing pipeline

    Parameters
    ----------
    text : str
        Text to be preprocessed.
    pos_tags : list
        List of part-of-speech tags you are interested in.
    """
    # Replace hyphen for underscore (single tokens)
    text = text.replace('-', '_')
    doc = nlp(text)
    result = []

    for token in doc:
        if token.pos_ in pos_tags and not token.is_stop:
            result.append(token.lemma_)

    return ' '.join(result)


# ============================================================
# Term(s) frequency analysis
# ============================================================
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer


def get_keywords(corpus, as_df=False, how='tfid', ngram_range=(2, 3), **kwargs):
    if isinstance(corpus, str):
        "If only a huge string is passed"
        corpus = [corpus]

    vectorizer = TfidfVectorizer(ngram_range=ngram_range, **kwargs)
    vectors = vectorizer.fit_transform(corpus)

    names = vectorizer.get_feature_names()
    data = vectors.todense().tolist()
    weights = vectorizer.idf_
    if as_df:
        name_series = pd.Series(names, name='Keywords', dtype='str')
        weight_series = pd.Series(weights, name='Weight')
        return pd.concat([name_series, weight_series], axis='columns').sort_values('Weight', ascending=False)
    else:
        return pd.Series(data=weights, index=names, name='Keywords').sort_values(ascending=False)


def get_textranks(text_series):
    """
    Computes a phrase ranking score based on the corpus given.

    References:

    - Mihalcea, Rada, and Paul Tarau. "Textrank: Bringing order into text." Proceedings of the 2004 conference on
    empirical methods in natural language processing. 2004.

    Parameters
    ----------
    text_series: pd.Series containing the corpus to be ranked

    Returns
    -------
    pd.DataFrame containing the top phrases, ranks, and frequency of them.

    """
    # Clean data
    clean_text = hero.clean(text_series)
    text = ' '.join(clean_text)

    # Extract keyword phrases
    doc = nlp(text)
    data_top = [{'rank': p.rank, 'count': p.count, 'text': p.text} for p in doc._.phrases]
    top_phrases = pd.DataFrame(data_top).sort_values('rank', ascending=False)

    return top_phrases


# ============================================================
# Dimensionality reduction
# ============================================================
from sklearn.decomposition import PCA


def latent_representation(docs, n_components=2, method='pca'):
    """Perform a latent representation of the documents extracted"""
    # X = pd.DataFrame()
    # for doc in docs:
    #     features = pd.Series(data=nlp(doc).vector, name=doc)
    #     X = X.append(features)
    # X_pca = PCA(n_components=n_components).fit_transform(X)
    # return pd.DataFrame(X_pca, columns=[f'Component {i}' for i in range(1, n_components + 1)], index=X.index)

    corpus = ' '.join(docs)
    doc = nlp(corpus)
    sentences = pd.Series(name='text', data=[sent.string.strip() for sent in doc.sents])
    sent_len = pd.Series(name='len', data=[len(sent.string.split()) for sent in doc.sents])

    # Some heuristics to define the relevant size of a sentence (ignore individual tokens)
    sentences = sentences[(sent_len > 10) & (sent_len < 40)].reset_index(drop=True)
    print(sentences)

    tf_idf = hero.tfidf(sentences, max_features=100)
    if method.lower() == 'pca':
        latent = hero.pca(tf_idf, n_components=n_components)
    elif method.lower() == 'tsne':
        latent = hero.tsne(tf_idf, n_components=n_components)
    else:
        raise NotImplementedError(f'Method "{method}" is not implemented.')
    # Make plot
    latent.name = method.upper()
    latent = latent.apply(lambda  x: pd.Series(x))
    latent.columns = [f'Component {i}' for i, _ in enumerate(latent.columns, 1)]
    return sentences.to_frame().join(latent)

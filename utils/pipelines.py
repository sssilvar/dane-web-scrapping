import validators

from .scrappers import scrap_site
from .nlp import get_keywords, get_textranks
from .ui import render_results, message_dialog


def run_pipeline(url=''):
    try:
        if not validators.url(url):
            return message_dialog('La URL ingresada no es correcta')

        results_series = scrap_site(url, timeout=20, sep_docs=True)

        # Extract Keywords
        keywords = get_textranks(results_series)

        # Parse dataframe as Dash object
        return render_results(keywords)
    except TypeError as e:
        return

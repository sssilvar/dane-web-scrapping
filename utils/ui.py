import  dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go


def pandas_to_markdown(series, n=10):
    mark_str = series.head(n).to_markdown()
    return dcc.Markdown(children=mark_str)

def plot_topics(series, n=5):
    sample = series.head(n).sort_index()
    keywords = sample.index
    values = sample.values

    # Create plot
    data = go.Scatterpolar(
        r=values,
        theta=keywords,
        fill='toself'
    )

    layout = go.Layout(polar=dict(radialaxis = dict(visible=True)),
                      showlegend = False)
    return dcc.Graph(id='polar-topics', figure={'data': [data], 'layout': layout})


def render_results(series):
    # Create tables for meningful and most frequent
    markdown_meaningful = pandas_to_markdown(series)
    markdown_frequent = pandas_to_markdown(series.sort_values()) # Inverts order 

    # Create plots
    polar_plot = plot_topics(series)

    # Create Divs
    div_meaningful = html.Div(children=[html.H4('Most meaningful'), markdown_meaningful], className='five columns')
    div_frequent = html.Div(children=[html.H4('Most frequent'), markdown_frequent], className='five columns')
    plot_div = html.Div(children=[polar_plot], className='twelve columns')

    return html.Div(children=[plot_div, div_meaningful, div_frequent], className='row')


def message_dialog(msg):
    return dcc.ConfirmDialog(id='message_dialog', message=msg)


def load_ui():
    input_url = dcc.Input(id='url', type='url', placeholder='Ingrese la URL a analizar', autoFocus=True, className='u-full-width')
    submit_btn = html.Button(id='submit-url', children='Analizar ahora', className='u-full-width button button-primary')

    input_div = html.Div(children=[input_url], className='nine columns')
    submit_div = html.Div(children=[submit_btn], className='three columns')
    return html.Div(id='input-ui', 
                    children=[input_div, submit_div],
                    className='row')


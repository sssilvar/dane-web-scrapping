#!/bin/env python3
from time import  sleep

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from utils.ui import load_ui, message_dialog
from utils.pipelines import run_pipeline

# Create app
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server  # For heroku

# ================
# Assembly layout
# ================
app.layout = html.Div(className='container', children=[
    html.H1(children='DANE Scrapper Analysis Toolkit'),

    html.Div(children='A tool to perform web scrapping and topic modeling.'),
    load_ui(),

    html.Div(children=[
        dcc.Loading(
            id="loading-1",
            type="default",
            children=html.Div(id="loading-output-1")
        ),
        html.H4(id='h3-message'),
    ]),
    html.Div(id='results')
    # run_pipeline()
])

# ==============================
# Add Callbacks
# ==============================
@app.callback([Output("loading-output-1", "children"), Output('results', 'children')],
              [Input('submit-url', 'n_clicks')],
              [State('url', 'value')])
def process_url(n_clicks, url):
    results = run_pipeline(url)

    loader_status = True
    return loader_status, results


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')
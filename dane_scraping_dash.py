import os
import validators
import streamlit as st
import plotly.express as px

@st.cache
def scrap_cached_site(url):
    return scrap_site(url)

"""
# Herramienta para analisis de sitios (webscraping)
Ésta herramienta permite analizar el contenido de sitios web.
"""
st.sidebar.image('https://www.dane.gov.co/templates/t3_bs3_blank/images/logo-DANE.png', use_column_width=True)
st.sidebar.title('Analizador de sitios DANE')

lang = st.sidebar.selectbox('¿Qué lenguaje desea utilizar?', options=['Inglés', 'Español'])

with st.spinner(f'Cargando modelo de deep learning ({lang})...'):
    from utils.nlp import get_keywords, latent_representation, reload_model, get_textranks
    reload_model(lang)
    from utils.scrappers import scrap_site


st.sidebar.subheader('Frases Significativas')
n_meaningful = st.sidebar.number_input('Número de frases', min_value=2, max_value=20, value=7, step=1, key='n-meaningful')


# Load url
url = st.text_input('Ingrese la URL a analizar y presione enter')
if validators.url(url):
    with st.spinner('Procesando website...'):
        text_series = scrap_cached_site(url)
        # st.write(' '.join(text_series))
        # keywords = get_keywords(site_raw_text, as_df=True)
        top_phrases = get_textranks(text_series)

        # st.subheader('Palabras más Frecuentes')
        # fig = px.bar(keywords[::-1].head(n_frequent), x='Keywords', y='Weight')
        # st.plotly_chart(fig, use_container_width=True)

        st.subheader('Frases más significativas')
        # fig = px.bar(keywords.head(n_meaningful), x='Keywords', y='Weight')
        fig = px.bar_polar(top_phrases.head(n_meaningful), r="count", theta="text", color="rank",
                           color_discrete_sequence=px.colors.sequential.Plasma_r)
        st.plotly_chart(fig, use_container_width=True)

        st.subheader('Analisis latente')
        latent_method = st.selectbox('Metodo de reduccion', options=['PCA', 'tSNE'])
        latent_space = latent_representation(text_series, method=latent_method)

        # Pot latent space
        latent_fig = px.scatter(latent_space, x='Component 1', y='Component 2', hover_name='text')
        latent_fig.update_traces(marker=dict(size=12,
                                      line=dict(width=2,
                                                color='DarkSlateGrey')),
                          selector=dict(mode='markers'))
        st.plotly_chart(latent_fig, use_container_width=True)

        if st.sidebar.checkbox('Ver texto bruto', False):
            st.write(text_series)

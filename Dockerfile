FROM python:3.7.4-slim-stretch

WORKDIR /

# The enviroment variable ensures that the python output is set straight
# to the terminal without buffering it first
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y procps libsm6 libxext6 libxrender-dev libglib2.0-0 git

COPY requirements.txt /requirements.txt
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt
RUN python -m spacy download en_core_web_md && python -m spacy download es_core_news_md

EXPOSE 80

# Configure streamlit
COPY .streamlit/config.prod.toml /root/.streamlit/config.toml
COPY .streamlit/credentials.prod.toml /root/.streamlit/credentials.toml

WORKDIR /app
COPY . ./

ENTRYPOINT [ "streamlit", "run", "dane_scraping_dash.py"]